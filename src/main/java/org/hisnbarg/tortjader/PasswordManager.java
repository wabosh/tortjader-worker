package org.hisnbarg.tortjader;

import org.hisnbarg.tortjader.Exceptions.PasswordsEmptyException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;

public class PasswordManager {

    private ConcurrentLinkedQueue<Password> passwords;

    private long size;

    public PasswordManager(File file) throws FileNotFoundException {
        Scanner scanner = new Scanner(file);
        passwords = new ConcurrentLinkedQueue<>();
        long i = 1;
        while (scanner.hasNext()) {
            passwords.add(new Password(scanner.next(), i++));
        }
        scanner.close();
        size = passwords.size();
    }

    public synchronized Password getPassword() throws PasswordsEmptyException {
        if (passwords.size() <= 0) {
            throw new PasswordsEmptyException();
        }
        return passwords.poll();
    }

    public synchronized void addPassword(Password password) {
        passwords.add(password);
    }

    public long size() {
        return size;
    }

}
