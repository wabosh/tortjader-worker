package org.hisnbarg.tortjader;

public class Password {

    private String password;
    private long id;

    public Password(String password, long id) {
        this.password = password;
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public long getId() {
        return id;
    }
}
