package org.hisnbarg.tortjader;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class MyCookieJar implements CookieJar {

    private List<Cookie> cookies;

    private final Logger logger = Logger.getLogger(getClass().getName());

    @NotNull
    @Override
    public List<Cookie> loadForRequest(@NotNull HttpUrl httpUrl) {
        if (cookies != null) {
            return cookies;
        }
        return new ArrayList<>();
    }

    @Override
    public void saveFromResponse(@NotNull HttpUrl httpUrl, @NotNull List<Cookie> list) {
        cookies = list;
    }

    public void clear() {
        cookies = null;
    }

    public boolean isSet() {
        return cookies != null;
    }

}
