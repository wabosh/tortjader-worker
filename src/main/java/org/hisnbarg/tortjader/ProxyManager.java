package org.hisnbarg.tortjader;

import org.hisnbarg.tortjader.Exceptions.ProxiesEmptyException;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ProxyManager {

    private ConcurrentLinkedQueue<Proxy> proxies;

    public ProxyManager(File file) throws FileNotFoundException {
        Scanner scanner = new Scanner(file);
        proxies = new ConcurrentLinkedQueue<>();
        while (scanner.hasNext()) {
            String[] lines = scanner.next().split(":");
            String host = lines[0];
            int port = Integer.parseInt(lines[1]);
            proxies.add(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(host, port)));
        }
        scanner.close();
    }

    public synchronized Proxy getProxy() throws ProxiesEmptyException {
        if (proxies.size() <= 0) {
            throw new ProxiesEmptyException();
        }
        Proxy proxy = proxies.poll();
        try {
            return proxy;
        } finally {
            proxies.add(proxy);
        }
    }

}
