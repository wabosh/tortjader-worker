package org.hisnbarg.tortjader;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.logging.Logger;

public class Main {

    private static Logger log = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws FileNotFoundException {
        log.info(String.format("Starting on %tc", new Date()));

        if (args.length != 3) {
            System.exit(1);
        }

        PasswordManager passwordManager = new PasswordManager(new File(args[0]));
        ProxyManager proxyManager = new ProxyManager(new File(args[1]));

        for (int i = 0; i < 40; i++) {
            Worker worker = new Worker(passwordManager, proxyManager, new File(args[2]));
            worker.start();
        }
    }

}
